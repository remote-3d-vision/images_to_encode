//#include "CLEyeMulticam.h"
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <process.h>
#include <math.h>
#include <vector>

#define __STDC_CONSTANT_MACROS

extern "C"
{

#include "libavcodec\avcodec.h"
#include "libavformat\avformat.h"

#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/channel_layout.h>
#include <libavutil/common.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/samplefmt.h>
#include <libswscale/swscale.h>

}
#include "encodeThread.h"

#include "image_buffer/image_buffer.cpp"

#pragma comment (lib, "avformat.lib")
#pragma comment (lib, "avcodec.lib")
#pragma comment (lib, "avutil.lib")
#pragma comment (lib, "swscale.lib")

using namespace std;

#define WITH_SYNCHRONIZATION
#define CODEC_ID CODEC_ID_MPEG4

//const char* output_filename = "udp://127.0.0.1:8888";
const int numCams = 2;

int main(int argc, const char** argv)
{
	bool output = false;
	char dump;

	
	//we will set up some basic Libav stuff to pass to the encoding threads
	AVCodec *codec;
	AVCodecContext *c = NULL;
	AVFormatContext *oc = NULL;
	AVStream *video_st = NULL;
	int i, ret, x, y, got_output;
	FILE *f;
	AVFrame *ff_frame, *raw_data;
	SwsContext *sws_ctx;
	AVPixelFormat src_camera_fmt = AV_PIX_FMT_BGRA, dst_codec_fmt = AV_PIX_FMT_YUV420P;

	image_buffer_reader * image_reader_left = new image_buffer_reader();
	image_buffer_reader * image_reader_right = new image_buffer_reader();
	image_info_t *image_info_left;
	image_info_t *image_info_right;

	//initialize our shared memory reader if there is shared memory available
	if (image_reader_left->init_left() < 0)
	{
		MessageBox(NULL, L"A program must open the shared memory first",
			L"Shared Memory Error",
			MB_OK | MB_ICONERROR);
		return -1;
	}
	if (image_reader_right->init_right() < 0)
	{
		MessageBox(NULL, L"A program must open the shared memory first",
			L"Shared Memory Error",
			MB_OK | MB_ICONERROR);
		return -1;
	}

	if (argc < 2)
	{
		cout << argv[0] << ": incorrect number of parameters" << endl;
		cout << "usage: " << argv[0] << " udp://<ip_addr>:<port>" << endl;
		exit(-1);
	}

	const char* output_filename = argv[1];

	av_register_all();

	codec = avcodec_find_encoder(CODEC_ID);
	if (!codec) {
		fprintf(stderr, "Codec not found\n");
		cin >> dump;
		exit(1);
	}
	c = avcodec_alloc_context3(codec);
	if (!c) {
		fprintf(stderr, "Could not allocate video codec context\n");
		cin >> dump;
		exit(1);
	}

	c->codec_id = CODEC_ID;
	if (argc > 2)
		c->bit_rate = atoi(argv[2]);
	else
		c->bit_rate = 5000000;
//	c->bit_rate_tolerance = 1000000000;
	/* resolution must be a multiple of two */
	c->width = 640;
	c->height = 480;
	/* frames per second */
	AVRational timebase;
	timebase.den = 60; timebase.num = 1;
	c->time_base = timebase;
	c->gop_size = 10; /* emit one intra frame every ten frames */
	c->has_b_frames = 0;
	//c->max_b_frames = 0;
	c->pix_fmt = dst_codec_fmt;

	avformat_alloc_output_context2(&oc, NULL, "mpegts", output_filename);
	if (!oc) {
		printf("Could not deduce output format from file name\n");
		cin >> dump;
		return(1);
	}
	avformat_network_init();
	av_dump_format(oc, 0, output_filename, 1);
	ret = avio_open(&oc->pb, output_filename, AVIO_FLAG_WRITE);
	if (ret < 0)
	{
		printf("could not open output\n");
		cin >> dump;
		exit(1);
	}


		cout << "Starting encoding threads..." << endl;
		i = 0;
		
		

		int frame_count = 1;
		AVPacket * pkt1 = new AVPacket, *pkt2 = new AVPacket;

		//set up threaded encoding classes, other variables
		encodeThread *thread1 = new encodeThread(oc, c, codec, image_reader_left, &pkt1);
		encodeThread *thread2 = new encodeThread(oc, c, codec, image_reader_right, &pkt2);
		unsigned uiThread1ID, uiThread2ID;
		HANDLE hth1, hth2;
		HANDLE readyHandles[2];
		HANDLE * ocMutex = new HANDLE;
		HANDLE * coutMutex = new HANDLE;
		HANDLE * beginProcessing = new HANDLE;
		HANDLE * beginProcessing1 = new HANDLE;

		//create thread
		hth1 = (HANDLE)_beginthreadex(NULL, 0, encodeThread::StaticEntryPoint, thread1, CREATE_SUSPENDED, &uiThread1ID);

		//set up mutex, events
		*(ocMutex) = CreateMutex(NULL, FALSE, TEXT("ocMutex"));
		*(coutMutex) = CreateMutex(NULL, FALSE, TEXT("coutMutex"));
		*(beginProcessing) = CreateEvent(NULL, TRUE, FALSE, TEXT("beginProcessing"));
		*(beginProcessing1) = CreateEvent(NULL, TRUE, FALSE, TEXT("beginProcessing1"));
		thread1->mutexes.ocMutex = ocMutex;
		thread1->mutexes.coutMutex = coutMutex;
		thread1->mutexes.beginProcessing = beginProcessing;
		//readyHandles[0] = thread1->mutexes.initializedEvent;
		
		//start thread 1
		ResumeThread(hth1);
		//cout << "waiting thread 1 init" << endl;
		WaitForSingleObject(thread1->mutexes.initializedEvent, INFINITE);
		//cout << "continue" << endl;

		hth2 = (HANDLE)_beginthreadex(NULL, 0, encodeThread::StaticEntryPoint, thread2, CREATE_SUSPENDED, &uiThread2ID);
		
		//initialize thread 2 mutex, events
		//readyHandles[1] = thread2->mutexes.initializedEvent;
		thread2->mutexes.ocMutex = ocMutex;
		thread2->mutexes.coutMutex = coutMutex;
		thread2->mutexes.beginProcessing = beginProcessing1;

		//start thread 2
		ResumeThread(hth2);
		//cout << "waiting thread 2 init" << endl;
		WaitForSingleObject(thread2->mutexes.initializedEvent, INFINITE);
		//cout << "continue"  << endl;


		//now that they're created, we can write the header (we don't necessarily care about
		//the header, but this call also allocates part of the stream)
		
		avformat_write_header(oc, NULL);
		//now we can tell both to begin
		SetEvent(*beginProcessing);
		SetEvent(*beginProcessing1);

		//required to handle keyboard input
		HANDLE stdInHandle = GetStdHandle(STD_INPUT_HANDLE);
		DWORD events;
		INPUT_RECORD keyBuffer;
		for (;;)
		{
			//we can sleep here because yay threading
			Sleep(50);

			//handle keyboard input
			PeekConsoleInput(stdInHandle, &keyBuffer, 1, &events);
			if (events > 0)
			{
				ReadConsoleInput(stdInHandle, (PINPUT_RECORD)&keyBuffer, 1, &events);
				dump = MapVirtualKeyW(keyBuffer.Event.KeyEvent.wVirtualKeyCode, MAPVK_VK_TO_CHAR);
			}

			if (dump == 'Q')
			{
				//tell threads to stop processing
				thread1->mutexes.processing_enabled = false;
				thread2->mutexes.processing_enabled = false;
				
				//wait for threads to finish
				readyHandles[0] = hth1;
				readyHandles[1] = hth2;
				WaitForMultipleObjects((numCams > 1 ? 2 : 1), readyHandles, TRUE, INFINITY);

				//close the codec, tidy up libav stuff
				avcodec_close(c);
				av_free(c);
				avio_close(oc->pb);
				avformat_network_deinit();
				avformat_free_context(oc);
				//exit
				return(0);
			}

		}
		return 0;
	}
