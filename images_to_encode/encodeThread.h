#define WITH_SYNCHRONIZATION

#ifndef _ENCODETHREAD_H_
#define _ENCODETHREAD_H_
#define CODEC_ID CODEC_ID_MPEG2VIDEO

#include "image_buffer/image_buffer.h"

class encodeThread_mutexPack
{
public:
	encodeThread_mutexPack::encodeThread_mutexPack(HANDLE * ocMutex_in = NULL, HANDLE * coutMutex_in = NULL)
	{
		initializedEvent = CreateEventW(NULL, FALSE, FALSE, TEXT("initializedEvent"));
		processing_enabled = true;
		ocMutex = ocMutex_in;
		coutMutex = coutMutex_in;

		ResetEvent(initializedEvent);
	}
	//required locks/signals:
	bool processing_enabled;
	//initialized
	HANDLE initializedEvent;
	//oc
	HANDLE * ocMutex;
	//cout
	HANDLE * coutMutex;
	//begin event
	HANDLE * beginProcessing;
};

class encodeThread
{
public:
	encodeThread(AVFormatContext *oc_in, AVCodecContext *c_in, AVCodec *codec_in, image_buffer_reader * image_reader_in, AVPacket** pkt_in);
	~encodeThread();
	static unsigned __stdcall encodeThread::StaticEntryPoint(void *pThis)
	{
		encodeThread * pThread = (encodeThread*)pThis;
		pThread->ThreadEntryPoint();
		return 1;
	}
	void ThreadEntryPoint();
	encodeThread_mutexPack mutexes;
private:
	AVFormatContext *oc;
	AVCodec *codec;
	AVStream *video_st;
	AVFrame *ff_frame, *raw_data;
	AVCodecContext *c;
	SwsContext *sws_ctx;
	image_buffer_reader * image_reader;
	image_info_t image_info;
	int frame_count;
	void init_objects();
	bool * process;
	AVPacket ** pkt;
	static const AVPixelFormat src_camera_fmt = AV_PIX_FMT_BGRA, dst_codec_fmt = AV_PIX_FMT_YUV420P;
};





#endif _ENCODETHREAD_H_