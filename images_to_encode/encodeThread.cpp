
//#include "CLEyeMulticam.h"
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <process.h>
#include <math.h>

#define __STDC_CONSTANT_MACROS

extern "C"
{

#include "libavcodec\avcodec.h"
#include "libavformat\avformat.h"

#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/channel_layout.h>
#include <libavutil/common.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/samplefmt.h>
#include <libswscale/swscale.h>

}

#include "encodeThread.h"
#include "image_buffer/image_buffer.h"

using namespace std;

encodeThread::encodeThread(AVFormatContext *oc_in, AVCodecContext *c_in, AVCodec *codec_in, image_buffer_reader * image_reader_in, AVPacket ** pkt_in)
{
	frame_count = 1;
	oc = oc_in;
	c = c_in;
	codec = codec_in;
	image_reader = image_reader_in;
	pkt = pkt_in;

	image_info.format = FORMAT_R8G8B8A8;
	image_info.height = c->height;
	image_info.width = c->width;
	image_info.size = image_info.width*image_info.height* 4;

}

encodeThread::~encodeThread()
{
	avcodec_close(video_st->codec);
	av_freep(&ff_frame->data[0]);
	av_frame_free(&ff_frame);
	av_freep(&raw_data->data[0]);
	av_frame_free(&raw_data);
}

//create encoding pipeline for this thread
void encodeThread::init_objects()
{
	int ret;
	char dump;
	//define the output frame container
	ff_frame = av_frame_alloc();
	if (!ff_frame) {
		WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
		fprintf(stderr, "Could not allocate video frame\n");
		ReleaseMutex(*(mutexes.coutMutex));
		exit(1);
	}
	ff_frame->format = c->pix_fmt;
	ff_frame->width = c->width;
	ff_frame->height = c->height;

	avcodec_align_dimensions2(c, &ff_frame->width, &ff_frame->height, ff_frame->linesize);

	//allocate the output frame
	ret = av_image_alloc(ff_frame->data, ff_frame->linesize, ff_frame->width, ff_frame->height, c->pix_fmt, 32);
	if (ret < 0) {
		fprintf(stderr, "Could not allocate raw picture buffer\n");
		cin >> dump;
		exit(1);
	}
	//configure the CSC/scaler (raw data will likely be in YUV420 along with the output, but just in case)
	sws_ctx = sws_getContext(c->width, c->height, src_camera_fmt, c->width, c->height, dst_codec_fmt, SWS_BILINEAR, NULL, NULL, NULL);
	if (!sws_ctx) {
		WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
		fprintf(stderr,
			"Impossible to create scale context for the conversion "
			"fmt:%s s:%dx%d -> fmt:%s s:%dx%d\n",
			av_get_pix_fmt_name(src_camera_fmt), c->width, c->height,
			av_get_pix_fmt_name(dst_codec_fmt), c->width, c->height);
		ReleaseMutex(*(mutexes.coutMutex));
		cin >> dump;
		exit(1);
	}

	//allocate & configure the input data container
	raw_data = av_frame_alloc();
	if (!raw_data) {
		WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
		fprintf(stderr, "Could not allocate video frame\n");
		ReleaseMutex(*(mutexes.coutMutex));
		exit(1);
	}
	raw_data->format = src_camera_fmt;
	raw_data->width = c->width;
	raw_data->height = c->height;

	ret = av_image_alloc(raw_data->data, raw_data->linesize, raw_data->width, raw_data->height, src_camera_fmt, 32);
	if (ret < 0) {
		WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
		fprintf(stderr, "Could not allocate raw capture buffer\n");
		ReleaseMutex(*(mutexes.coutMutex));
		cin >> dump;
		exit(1);
	}

	WaitForSingleObject(*(mutexes.ocMutex), INFINITE);
	//create a stream for this encoding thread
	video_st = avformat_new_stream(oc, codec);
	if (!video_st) {
		WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
		fprintf(stderr, "Could not allocate stream\n");
		ReleaseMutex(*(mutexes.coutMutex));
		cin >> dump;
		exit(1);
	}
	
	//set it to the correct stream ID
	video_st->id = oc->nb_streams - 1;
	ReleaseMutex(*(mutexes.ocMutex));

	video_st->stream_identifier = video_st->id;

	//copy the codec data from the primary CodecContext we were provided by the main thread
	video_st->codec->codec_id = c->codec_id;
	video_st->codec->bit_rate = c->bit_rate;
	video_st->codec->width = c->width;
	video_st->codec->height = c->height;
	video_st->codec->time_base = c->time_base;
	video_st->codec->gop_size = c->gop_size;
	video_st->codec->has_b_frames = c->has_b_frames;
	video_st->codec->pix_fmt = dst_codec_fmt;

	//now that we've used the data passed by the primary CodecContext, we can switch
	//to using the one contained by the video stream video_st and open it
	c = video_st->codec;
	if (avcodec_open2(c, codec, NULL) < 0) {
		fprintf(stderr, "Could not open codec\n");
		cin >> dump;
		exit(1);
	}

	WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
	cout << "initialized thread " << video_st->index << " ..." << endl;
	ReleaseMutex(*(mutexes.coutMutex));

	if (!SetEvent(mutexes.initializedEvent))
	{
		WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
		printf("error setting initialized event\n");
		ReleaseMutex(*(mutexes.coutMutex));
	}

}

void encodeThread::ThreadEntryPoint()
{
	int ret = 0, got_output;
	init_objects();
	
	DWORD dwWaitResult;
	WaitForSingleObject(*(mutexes.beginProcessing), INFINITE);

	dwWaitResult = WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
	cout << "encode thread " << video_st->index << " encoding" << endl;
	if (!ReleaseMutex(*(mutexes.coutMutex)))
	{
		printf("error releasing mutex\n");
		return;
	}
	while (mutexes.processing_enabled)
	{
		//initialize a packet to hold some compressed data
		av_init_packet(*pkt);

		(*pkt)->data = NULL; // packet data will be allocated by the encoder
		(*pkt)->size = 0;
		(*pkt)->stream_index = video_st->index;
		image_info_t * infoptr = &image_info;
		
		//capture some data from the image buffer
		//this is pretty ugly but must be done with the current image buffer implementation
		image_reader->get_image_buffer(&infoptr, (uint8_t **)(raw_data->data));
		
		//ensure data is in the appropriate format & layout
		ret = sws_scale(sws_ctx, (const uint8_t * const *)raw_data->data, raw_data->linesize, 0, raw_data->height, ff_frame->data, ff_frame->linesize);
		if (ret < 0) {
			dwWaitResult = WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
			fprintf(stderr, "Error during colour space conversion\n");
			ReleaseMutex(*(mutexes.coutMutex));
			getchar();
			break;
		}
		
		ff_frame->pts = frame_count;
		//encode the frame
		ret = avcodec_encode_video2(c, (*pkt), ff_frame, &got_output);
		if (ret < 0) {
			dwWaitResult = WaitForSingleObject(*(mutexes.coutMutex), INFINITE);
			fprintf(stderr, "Error encoding frame\n");
			ReleaseMutex(*(mutexes.coutMutex));
			getchar();
			break;
		}
		if (got_output) {
			//ensure the packet timebases are correct so the decoder can decode them
			(*pkt)->pts = av_rescale_q_rnd((*pkt)->pts, c->time_base, video_st->time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
			(*pkt)->dts = av_rescale_q_rnd((*pkt)->dts, c->time_base, video_st->time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
			(*pkt)->duration = av_rescale_q((*pkt)->duration, c->time_base, video_st->time_base);
			dwWaitResult = WaitForSingleObject(*(mutexes.ocMutex), INFINITE);
			if (dwWaitResult == WAIT_ABANDONED)
			{
				printf("error waiting for mutex: abandoned mutex\n");
				getchar();
				break;
			}
			//place the newly-compressed frame on the correct stream
			(*pkt)->stream_index = video_st->index;
			//write the frame
			av_interleaved_write_frame(oc, *pkt);
			if (!ReleaseMutex(*(mutexes.ocMutex)))
			{
				printf("error releasing mutex\n");
				getchar();
				break;
			}

		}
		//NOTE: this used to be above, in if(got_output)
		av_free_packet(*pkt);

		frame_count++;
	}
}